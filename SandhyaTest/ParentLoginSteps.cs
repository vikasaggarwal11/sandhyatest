﻿using NUnit.Framework;
using SandhyaTest.DataModel;
using SandhyaTest.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SandhyaTest
{
    [Binding]
    public sealed class ParentLoginSteps : SandhyaTest.Hooks1
    {

        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef
        [Given(@"I am on the login page")]
        public void GivenIAmOnTheLoginPage()
        {
          
        }
        [When(@"I enter correct username and password")]
        public void WhenIEnterCorrectUsernameAndPassword(Table table)
        {
            ParentLoginPage loginPage = new ParentLoginPage(driver);
            Parent parent = table.CreateInstance<Parent>();
            loginPage.doLogin(parent);
        }
        [Then(@"I should be able to login successfully")]
        public void ThenIShouldBeAbleToLoginSuccessfully()
        {
            ParentDashboard parentDashboard = new ParentDashboard(driver);
            Assert.AreEqual("AutomationFN",parentDashboard.getChildName());

        }


    }
}

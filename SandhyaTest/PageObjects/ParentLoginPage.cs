﻿using OpenQA.Selenium;
using SandhyaTest.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SandhyaTest.PageObjects
{
    public class ParentLoginPage
    {
        IWebDriver _driver;
        public ParentLoginPage(IWebDriver driver)
        {
            this._driver = driver;
        }

        private IWebElement username()
        {
            return _driver.FindElement(By.CssSelector("#Email"));
        }

        private IWebElement password()
        {
            return _driver.FindElement(By.CssSelector("#Password"));
        }
        private IWebElement loginButton()
        {
            return _driver.FindElement(By.CssSelector("#Login"));
        }
        public void doLogin(Parent parent)
        {
            username().SendKeys(parent.username);
            password().SendKeys(parent.password);
            loginButton().Click();
        }
    }
}

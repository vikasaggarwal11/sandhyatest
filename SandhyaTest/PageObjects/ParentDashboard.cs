﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SandhyaTest.PageObjects
{
    public class ParentDashboard
    {
        IWebDriver _driver;
       public ParentDashboard(IWebDriver driver)
        {
            this._driver = driver;
        }

        private IWebElement childNameLabel()
        {
            return _driver.FindElement(By.CssSelector(".tb-profile__nickname"));

        }
        public string getChildName()
        {
          return  childNameLabel().Text;
        }
    }

}
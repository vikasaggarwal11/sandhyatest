﻿Feature: login
	In order to use tudubuddy app 
	As a parent
	I want to be logged in successfully

@mytag
Scenario: Parent Login
	Given I am on the login page
	When I enter correct username and password
	| username                       | password    |
	| automationtester@tudubuddy.com | Password 00 |
	Then I should be able to login successfully
											  